# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160806074655) do

  create_table "horses", force: true do |t|
    t.integer  "race_id"
    t.integer  "horse_id"
    t.integer  "wakuban"
    t.integer  "umaban"
    t.string   "bamei"
    t.integer  "ninki"
    t.float    "odds"
    t.string   "kishu"
    t.string   "sex"
    t.integer  "barei"
    t.float    "juryo"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "past_races", force: true do |t|
    t.integer  "horse_id"
    t.integer  "past_race_id"
    t.integer  "order"
    t.integer  "tousu"
    t.float    "time_dif"
    t.float    "juryo"
    t.string   "kishu"
    t.string   "keibajo"
    t.string   "course"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "raceName"
    t.string   "className"
  end

end
