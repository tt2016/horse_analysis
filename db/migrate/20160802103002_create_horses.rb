class CreateHorses < ActiveRecord::Migration
  def change
    create_table :horses do |t|
      t.integer :race_id
      t.integer :horse_id
      t.integer :wakuban
      t.integer :umaban
      t.string :bamei
      t.integer :ninki
      t.float :odds
      t.string :kishu
      t.string :sex
      t.integer :barei
      t.float :juryo

      t.timestamps
    end
  end
end
