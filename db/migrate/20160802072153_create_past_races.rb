class CreatePastRaces < ActiveRecord::Migration
  def change
    create_table :past_races do |t|
      t.integer :horse_id
      t.integer :past_race_id
      t.integer :order
      t.integer :tousu
      t.float :time_dif
      t.float :juryo
      t.string :kishu
      t.string :keibajo
      t.string :course

      t.timestamps
    end
  end
end
