class AddClassNameToPastRace < ActiveRecord::Migration
  def change
    add_column :past_races, :className, :string
  end
end
