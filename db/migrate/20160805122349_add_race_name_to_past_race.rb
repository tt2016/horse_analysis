class AddRaceNameToPastRace < ActiveRecord::Migration
  def change
    add_column :past_races, :raceName, :string
  end
end
