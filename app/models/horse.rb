#出走馬情報を保持するクラス
class Horse < ActiveRecord::Base
  
  #初期処理
  def initialize
    super
    @pastRaces = Array.new
    @score = 0
    @rating = String.new
    @result = 0
  end

  #過去レース情報を取得する  
  def pastRaces
    @pastRaces
  end

  #指数を取得する  
  def score
    @score
  end

  #推奨度を取得する  
  def rating
    @rating
  end

  #順位を取得する  
  def result
    @result
  end

  #過去レース情報を設定する  
  def pastRaces=(value)
    @pastRaces = value
  end

  #指数を設定する  
  def score=(value)
    @score = value
  end

  #推奨度を設定する  
  def rating=(value)
    @rating = value
  end

  #順位を設定する  
  def result=(value)
    @result = value
  end

end
