class Race
  
  def initialize
    super
    @raceName = String.new
    @horseCnt = 0
    @horses = Array.new
    @keibajo = String.new
    @course = String.new
    @className = String.new
    @classCode = 0
    @ratedHorse = Array.new
  end
  
  def raceName
    @raceName
  end

  def horseCnt
    @horseCnt
  end

  def horses
    @horses
  end

  def keibajo
    @keibajo
  end

  def course
    @course
  end

  def className
    @className
  end

  def classCode
    @classCode
  end

  def ratedHorse
    @ratedHorse
  end

  def raceName=(value)
    @raceName = value
  end
  
  def horseCnt=(value)
    @horseCnt = value
  end

  def horses=(value)
    @horses = value
  end
  
  def keibajo=(value)
    @keibajo = value
  end

  def course=(value)
    @course = value
  end

  def className=(value)
    @className = value
  end

  def classCode=(value)
    @classCode = value
  end

  def ratedHorse=(value)
    @ratedHorse = value
  end

end
