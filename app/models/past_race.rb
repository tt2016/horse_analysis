#出走馬の過去レースの情報を保持するクラス
class PastRace < ActiveRecord::Base
  
  #初期処理
  def initialize
    super
    @raceValid = true
    @score = 0
    @classCode = 0
  end
  
  #指数算出への使用可否を取得する
  def raceValid
    @raceValid
  end

  #指数を取得する
  def score
    @score
  end

  #クラスコードを取得する
  def classCode
    @classCode
  end

  #指数算出への使用可否を設定する
  def raceValid=(value)
    @raceValid = value
  end

  #指数を設定する
  def score=(value)
    @score = value
  end
  
  #クラスコードを設定する
  def classCode=(value)
    @classCode = value
  end

end
