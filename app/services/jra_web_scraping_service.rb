require 'open-uri'
require 'nokogiri'
require 'capybara-webkit'

#JRAのホームページから出走馬の情報をWebスクレイピングにより取得するクラス
class JRAWebScrapingService
  #初期処理
  def initialize

    begin
      Capybara::Webkit.configure do |config|
        #不要なログを非表示にする
        config.allow_unknown_urls
      end
    rescue
      #初回のみ実行
    end

    Capybara.javascript_driver = :webkit
    #セレクタをxpathに設定
    Capybara.default_selector = :xpath
  end

  #開催名を取得する
  # ==== Args
  # idx :: 開催日（１or２）
  # ==== Return
  # 開催名
  def getKaisaiName(idx)

    #画面遷移
    s = Capybara::Session.new(:webkit)
    s.visit 'http://www.jra.go.jp'

    #出馬表ボタンを押下
    s.find('//a[text()="出馬表"]').click

    #レース情報の取得
    doc = Nokogiri::HTML.parse(s.html)

    #開催名
    kaisaiName = doc.xpath("(//table[@class='kaisaiDay" + idx + "'])[1]/descendant::node()/th[1]").inner_text

    pid = s.driver.inspect.scan(/@pid=(\d+)/).flatten.first
    `kill -9 #{pid}`
    return kaisaiName
  end

  #レース名の一覧を取得する
  # ==== Args
  # idx :: 開催日（１or２）
  # ==== Return
  # レース名の一覧
  def getRaceName(idx)

    #画面遷移
    s = Capybara::Session.new(:webkit)
    s.visit 'http://www.jra.go.jp'

    #出馬表ボタンを押下
    s.find('//a[text()="出馬表"]').click

    #レース情報の取得
    doc = Nokogiri::HTML.parse(s.html)
    raceNames = Array.new
    doc.xpath("(//table[@class='kaisaiDay" + idx + "'])[1]/descendant::node()/td[@class='kaisaiBtn']").each do |node|
      raceNames.push(node.xpath('./a[1]').inner_text)
    end

    pid = s.driver.inspect.scan(/@pid=(\d+)/).flatten.first
    `kill -9 #{pid}`
    return raceNames
  end

  #レース全体の情報を取得する
  # ==== Args
  # kaisaiName :: 開催名
  # raceId :: レース番号
  # ==== Return
  # レース情報
  def getRaceData(kaisaiName, raceId)

    #画面遷移
    s = Capybara::Session.new(:webkit)
    s.visit 'http://www.jra.go.jp'

    #出馬表ボタンを押下
    s.find('//a[text()="出馬表"]').click

    #開催ボタンを押下
    doc = Nokogiri::HTML.parse(s.html)
    idx = 1
    doc.xpath('//td[@class="kaisaiBtn"]').each do |node|
      if node.xpath('./a').inner_text == kaisaiName then
        s.find("(//td[@class='kaisaiBtn'])[" + idx.to_s + "]/a").click
      end
      idx += 1
    end

    #レース番号を押下
    s.find("(//img[@alt='" + raceId + "R'])/parent::node()").click

    #レース情報の取得
    doc = Nokogiri::HTML.parse(s.html)

    #レース情報の初期化
    race = Race.new

    #レース名
    race.raceName = doc.xpath('//p[@class="specialRace"]').inner_text
    p race.raceName

    #クラス名
    if race.raceName.include?("ＧＩＩＩ") then
      #G3
      race.className = "G3"
    elsif race.raceName.include?("ＧＩＩ") then
      #G2
      race.className = "G2"
    elsif race.raceName.include?("ＧＩ") then
      #G1
      race.className = "G1"
    else
      race.className = doc.xpath('//td[@class="raceData"][1]/descendant::node()/td').first.inner_text
    end
    race.classCode = getClassCode(race.className)

    if race.classCode == -1 || race.classCode == 1 then
      pid = s.driver.inspect.scan(/@pid=(\d+)/).flatten.first
      `kill -9 #{pid}`
      return race
    end

    #競馬場
    race.keibajo = doc.xpath('//td[@class="header3"][1]').inner_text.split(" ")[1].split("回")[1][0,2]
    race.course = doc.xpath('//td[@class="raceData"][1]/descendant::node()/td[2]').first.inner_text.split("　")[0].sub("m", "") \
    + doc.xpath('//td[@class="raceData"][1]/descendant::node()/td[2]').first.inner_text.split("　")[1][0]

    #出走頭数の取得
    race.horseCnt = doc.xpath('//td[@class="hNumber"]').count

    race.horseCnt.times do |horse_idx|
      #出走馬情報の取得（出走頭数分）

      #出走馬モデルの生成
      horse = Horse.new

      #起点の位置情報
      horseNode = doc.xpath("(//td[@class='hNumber'])[" + (horse_idx + 1).to_s + "]/parent::node()")

      #馬番
      horse.umaban = horseNode.xpath('./td[2]').first.inner_text.to_i

      #枠番
      begin
        horse.wakuban = horseNode.xpath('./td[1]').css('img').attribute('alt').value.to_i
      rescue
        horse.wakuban = nil
        horse.umaban = nil
      end

      #馬名
      horse.bamei = horseNode.xpath('./td[3]/descendant::node()/td[@class="horseName"]/a').first.inner_text
      p horse.bamei

      #人気
      begin
        horse.ninki = horseNode.xpath('./td[3]/descendant::node()/span[@class="ninki"]').first.inner_text.sub("(", "").sub("人気)", "").to_i #TODO 販売開始前はこの項目を非表示にする
      rescue
        horse.ninki = nil
      end

      #オッズ
      begin
        horse.odds = horseNode.xpath('./td[3]/descendant::node()/td[@class="horseName"]/span[@class="detailCells"]').first.inner_text.to_f
      rescue
        horse.odds = nil
      end

      #騎手
      horse.kishu = horseNode.xpath('./td[4]/descendant::node()/a').first.inner_text

      #性/齢/負担重量
      tmp = horseNode.xpath('./td[4]/descendant::node()/td[@align="center"]').first.inner_text

      column = tmp.split(" ")

      #性別/馬齢
      if column[0][0] == "牡" or column[0][0] == "牝" then
        horse.sex = column[0][0]
        horse.barei = column[0][1].to_i
      else
        horse.sex = "せ"
        horse.barei = column[0][2].to_i
      end

      #負担重量(kgを除去＋数値化)
      horse.juryo = column[2].gsub("kg", "").to_f

      race_idx = 5

      4.times do |idx|

        #過去レースモデルの生成
        pastRace = PastRace.new

        #順位
        begin
          pastRace.order = horseNode.xpath("./td[" + race_idx.to_s + "]/descendant::node()/td[1]/descendant::node()/td[@class='order']").first.inner_text.to_i
        rescue NoMethodError => ex
          pastRace.raceValid = false;
          horse.pastRaces.push(pastRace)
          race_idx += 1
          next
        end

        #出走頭数・人気
        #p horseNode.xpath("./td[" + race_idx.to_s + "]/descendant::node()/td[1]/descendant::node()/td[@class='order']/../td[2]").first.inner_text
        pastRace.tousu = horseNode.xpath("./td[" + race_idx.to_s + "]/descendant::node()/td[1]/descendant::node()/td[@class='order']/../td[2]").first.inner_text.split("\n")[2].sub("頭 ", "").to_i

        #タイム差
        begin
          time_begin = horseNode.xpath("./td[" + race_idx.to_s + "]/descendant::node()/tr[4]/td").first.inner_text.index("(")
          time_end = horseNode.xpath("./td[" + race_idx.to_s + "]/descendant::node()/tr[4]/td").first.inner_text.index(")")
          pastRace.time_dif = horseNode.xpath("./td[" + race_idx.to_s + "]/descendant::node()/tr[4]/td").first.inner_text[time_begin + 1, time_end - time_begin - 1 ].to_f
        rescue
          #競走中止
          pastRace.time_dif = 99
        end

        #負担重量
        pastRace.juryo = horseNode.xpath("./td[" + race_idx.to_s + "]/descendant::node()/tr[4]/td").first.inner_text.split(" ")[2].to_f

        #競馬場
        pastRace.keibajo = horseNode.xpath("./td[" + race_idx.to_s + "]/descendant::node()/td[@class='raceName']/../../tr[1]/td[2]").first.inner_text

        #コース
        pastRace.course = horseNode.xpath("./td[" + race_idx.to_s + "]/descendant::node()/tr[4]/td").first.inner_text.split(" ")[3]

        #クラス
        #パターン1（条件戦）
        pastRace.className = horseNode.xpath("./td[" + race_idx.to_s + "]/descendant::node()/td[@class='raceName']/a[1]").inner_text.sub("牝", "")
        pastRace.classCode = getClassCode(pastRace.className)

        #パターン2（特別レース）
        if pastRace.classCode == 0 then
          pastRace.className = horseNode.xpath("./td[" + race_idx.to_s + "]/descendant::node()/td[@class='raceName']/../td[2]").inner_text.sub("牝", "")
          pastRace.classCode = getClassCode(pastRace.className)
        end

        #パターン3（重賞）
        if pastRace.classCode == 0 then
          begin
            pastRace.className = horseNode.xpath("./td[" + race_idx.to_s + "]/descendant::node()/td[@class='raceName']/../td[2]").css('img').attribute('alt').value
            pastRace.classCode = getClassCode(pastRace.className)
          rescue
            pastRace.classCode = 0
            pastRace.raceValid = false;
          end
        end

        #パターン4（障害）
        if pastRace.classCode == 0 then
          pastRace.className = "障害"
          pastRace.classCode = 0
          pastRace.raceValid = false;
        end

        #出走馬情報に過去レース情報を追加
        horse.pastRaces.push(pastRace)
        race_idx += 1
      end

      race.horses.push(horse)
    end

    pid = s.driver.inspect.scan(/@pid=(\d+)/).flatten.first
    `kill -9 #{pid}`
    return race
  end
  
  #レース全体の情報を取得する
  # ==== Args
  # kaisaiName :: 開催名
  # raceId :: レース番号
  # ==== Return
  # レース情報
  def getResult(kaisaiName, raceId, bamei)
    #画面遷移
    s = Capybara::Session.new(:webkit)
    s.visit 'http://www.jra.go.jp'
  
    #出馬表ボタンを押下
    s.find('//a[text()="レース結果"]').click
  
    #開催ボタンを押下
    doc = Nokogiri::HTML.parse(s.html)
    idx = 1
    doc.xpath('//td[@class="kaisaiBtn"]').each do |node|
      if node.xpath('./a').inner_text.sub("\n", "") == kaisaiName then
        s.find("(//td[@class='kaisaiBtn'])[" + idx.to_s + "]/a").click
        break
      end
      idx += 1
    end
  
    #レース番号を押下
    s.find("(//img[@alt='" + raceId + "R'])/parent::node()").click
    
    #レース情報の取得
    doc = Nokogiri::HTML.parse(s.html)

    #馬名のリストを取得
    horseNames = doc.xpath('//td[@class="gray12"]/a').inner_text

    result = 0
    doc.xpath('//td[@class="gray12"]/a').each do |node|
      if node.inner_text == bamei then 
        result = node.xpath('./parent::node()/parent::node()/td[1]').inner_text.sub(" ", "").to_i
      end
    end
    
    pid = s.driver.inspect.scan(/@pid=(\d+)/).flatten.first
    `kill -9 #{pid}`
    return result
  end  

  #クラス名をコードに変換する
  # ==== Args
  # className :: クラス名
  # ==== Return
  # クラスコード
  #  新馬：1、未勝利：2、500万下：3、1000万下：4、1600万下：5、
  #  オープン：6、G3：7、G2：8、G1：9、
  #  障害：-1、不明：0
  def getClassCode(className)

    case className
    when /新馬/
      return 1
    when /未勝利/
      return 2
    when /500/
      return 3
    when /1000/
      return 4
    when /1600/
      return 5
    when /OP/
      return 6
    when /ＯＰ/
      return 6
    when /G3/
      return 7
    when /G2/
      return 8
    when /G1/
      return 9
    when /障害/
      return -1
    else
      return 0
    end

  end

end
