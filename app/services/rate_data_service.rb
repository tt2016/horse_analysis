#出走馬の指数と推奨度を算出するクラス
class RateDataService
  
  #初期処理
  # ==== Args
  # race :: レース情報
  def initialize(race)
    @race = race
  end

  #指数と推奨度を算出する
  def getRatedData

    if @race.classCode == -1 || @race.classCode == 1 then
      #障害レース／新馬戦は指数を算出できない
      return
    end 
    
    @race.horses.each do |horse|

      p horse.bamei
      
      pastScore = 0
      pastValidCnt = 0

      horse.pastRaces.each do |pastRace|
        #過去レースの指数を算出

        if pastRace.raceValid == false then
          #指数にカウントしない無効なレース
          next
        end

        #過去レースの指数算出
        score= 0

        #着順
        case pastRace.order
        when 1
          score += 10
        when 2
          score += 8
        else

          #タイム差
          if pastRace.order >= 2 then
            case pastRace.time_dif
            when 0.1..0.3
              score += 7
            when 0.4, 0.5
              score += 5
            else
              if pastRace.order.between?(3, 5) then
                #掲示板確保
                score += 5
              else
                score += 3
              end
            end
          end
        end

        #コース・クラス：

        if score > 7 then
          #　・過去レースのクラスが下の場合は、-2ポイント（新馬・未勝利は除外）
          if @race.classCode > 2 && pastRace.classCode < @race.classCode then
            score += -2
          end
          
          #　・当レースと芝／ダートが異なる場合は、-2ポイント
          if @race.course[4,1] != pastRace.course[4,1] then
            score += -2
          end

          #　・当レースと距離が異なる場合は、-1ポイント
          if @race.course[0,4] != pastRace.course[0,4] then
            score += -1
          end
        end

        p score
        pastRace.score = score
        pastScore += score
        pastValidCnt += 1
      end

      #過去レースの平均指数を算出
      if pastValidCnt > 0 then
        print "平均："
        horse.score = pastScore / pastValidCnt
        p horse.score
      end
      
      #推奨度を設定
      if horse.odds == 0 then 
        #出走
        horse.rating = ""
      else
        if horse.score >= 7 then
          if horse.odds >= 5 then
            #指数が7以上かつオッズが5倍以上＝A
            horse.rating = "A"
            @race.ratedHorse.push(horse)
          else
            #指数が7以上かつオッズが5倍未満＝B
            horse.rating = "B"
            @race.ratedHorse.push(horse)
          end
        elsif horse.score >= 6
          if horse.odds >= 10 then
            #指数が6以上かつオッズが10倍以上＝B
            horse.rating = "B"
          else
            horse.rating = "C"
          end
        else
          #指数が6未満＝D
          horse.rating = "D"
        end
      end
    end
  end
end