#レース情報を表示するコントローラクラス
class RaceAnalysisController < ApplicationController

  #レース情報を表示する
  def show

    #Webスクレイピング情報をもとに、解析クラスを呼び出す
    @race = JRAWebScrapingService.new.getRaceData(params[:raceName], params[:raceId])

    #指数と推奨度を計算する
    RateDataService.new(@race).getRatedData
  end
  
end