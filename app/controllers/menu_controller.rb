#レース一覧を表示するコントローラクラス
class MenuController < ApplicationController
  
  #レース一覧を表示する
  def show

    #開催日１
    @kaisaiName1 = JRAWebScrapingService.new.getKaisaiName("1")
    @raceNames1 = JRAWebScrapingService.new.getRaceName("1")
    @kaisais1 = Array.new
    
    @ratedACount1 = 0
    @ratedBCount1 = 0
    @ratedAWinCount1 = 0
    @ratedBWinCount1 = 0
    @ratedAFukuCount1 = 0
    @ratedBFukuCount1 = 0
    
    @raceNames1.each do |raceName|
      races = Array.new
      12.times do |idx|
        #Webスクレイピング情報をもとに、解析クラスを呼び出す
        race = JRAWebScrapingService.new.getRaceData(raceName, (idx + 1).to_s)
    
        #指数と推奨度を計算する
        RateDataService.new(race).getRatedData

        race.ratedHorse.each do |horse|
          @ratedBCount1 += 1
          if horse.rating == "A" then
            @ratedACount1 += 1
          end
          #推奨度が高い馬の結果を取得する
          horse.result = JRAWebScrapingService.new.getResult(raceName, (idx + 1).to_s, horse.bamei)

          if horse.result == 1 then
            @ratedBWinCount1 += 1
            if horse.rating == "A" then
              @ratedAWinCount1 += 1
            end
          end
          
          if horse.result <= 3 then
            @ratedBFukuCount1 += 1
            if horse.rating == "A" then
              @ratedAFukuCount1 += 1
            end
          end
          
        end
        
        races.push(race)
      end
      
      @kaisais1.push(races)
      
    end

    #開催日２
    @kaisaiName2 = JRAWebScrapingService.new.getKaisaiName("2")
    @raceNames2 = JRAWebScrapingService.new.getRaceName("2")
    @kaisais2 = Array.new
    
    @ratedACount2 = 0
    @ratedBCount2 = 0
    @ratedAWinCount2 = 0
    @ratedBWinCount2 = 0
    @ratedAFukuCount2 = 0
    @ratedBFukuCount2 = 0
    
    @raceNames2.each do |raceName|
      races = Array.new
      12.times do |idx|
        #Webスクレイピング情報をもとに、解析クラスを呼び出す
        race = JRAWebScrapingService.new.getRaceData(raceName, (idx + 1).to_s)
    
        #指数と推奨度を計算する
        RateDataService.new(race).getRatedData

        race.ratedHorse.each do |horse|
          @ratedBCount2 += 1
          if horse.rating == "A" then
            @ratedACount2 += 1
          end
          #推奨度が高い馬の結果を取得する
          horse.result = JRAWebScrapingService.new.getResult(raceName, (idx + 1).to_s, horse.bamei)

          if horse.result == 1 then
            @ratedBWinCount2 += 1
            if horse.rating == "A" then
              @ratedAWinCount2 += 1
            end
          end
          
          if horse.result <= 3 then
            @ratedBFukuCount2 += 1
            if horse.rating == "A" then
              @ratedAFukuCount2 += 1
            end
          end
          
        end
        
        races.push(race)
      end
      
      @kaisais2.push(races)
      
    end
    
  end  

end
