== README

* システム概要
 JRA(日本中央競馬会のサイト(http://www.jra.go.jp)の出馬表ページをWebスクレイピングにより解析し、
 レースに出走する馬の能力値（指数）を算出、オッズと比較した推奨度を表示する。
 さらに、推奨度が高い馬についてJRAのレース結果ページから着順を取得し、的中率を算出する。
 以上により、指数算出ロジックの検証を行う。
 Webスクレイピングにはnokogiri(http://www.nokogiri.org)を使用している。

* 動作保証環境
 Ruby 2.3.0
 Rails 4.0.2
 nokogiri 1.6.8
 Capybara-webkit 1.11.1
 
Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.
